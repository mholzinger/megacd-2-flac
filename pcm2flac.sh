#!/bin/bash

files=(*.bin)
target_format=flac

echo "XLD will attempt to convert [`expr ${#files[@]} - 1`] files to $target_format"
for (( n = 0; n < ${#files[@]}; n++ ))
do
  pcm="${files[$n]}"

  # We are skipping the first track, which is a Binary track, containing the game data
  if [[ "$pcm" =~ "${files[0]}" ]]; then
    continue
  fi

  echo "Info: Converting [$n of `expr ${#files[@]} - 1`]"
  echo "["$pcm"]"

  # Invoke XLD and convert the PCM to FLAC
  /Applications/XLD/CLI/xld \
    --raw \
    --samplerate=44100 --bit=16 --channels=2 --endian=little \
    "$pcm" \
    -f $target_format \
    -o .
done
