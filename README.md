# MegaCD 2 FLAC

Convert MegaCD PCM audio files to FLAC with XLD

A large part of the MegaCD library uses RedBook audio for the soundtrack medium. 
Most ripped tracks are typically in .BIN (PCM) format, and can be converted to FLAC

With XLD installed to `/Applications` on OSX, the XLD cli shell app will be invoked from `/Applications/XLD/CLI/xld`

